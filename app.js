const express = require('express');
let app = express();
const PORT = 8080;
const bootstrap = require('./src/bootstrap');

//The ability to read the request body
app.use(express.json());

const router = express.Router();
app.use(router);
bootstrap(app, router);

router.get('/', (req, res, next) => {
    return res.send('Good morning');
});


app.listen(PORT, err => {
    if (err) {
        return console.log('Cannot listen on port %s', PORT);
    }
    console.log('Server is listening on: http://localhost:%s/', PORT);
});