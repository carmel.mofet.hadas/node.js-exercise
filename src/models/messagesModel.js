//const { countReset } = require('console');
//const { readFile } = require('fs');
fs = require('fs');


exports.getMessages = () => {
  let messages = [];
  if (fs.existsSync('./src/db.json')) {
    messages = require('../db.json');
  } else {
    console.log('The database file does not exist');
  };
  
  return messages;
};

//Search for the messages of a specific user in the database
exports.getUserMessages = (username) => {

  let messages = [];
  if (fs.existsSync('./src/db.json')) {
    messages = require('../db.json');
  } else {
    console.log('The database file does not exist');
  };

  let userMessages = [];
  let data = [];

  //Check for each message if the receiver is the given user
  if (messages != []) {
    messages.forEach((message) => {
    
      if (message.receiver == username) {
  
        userMessages.push(message);
        message.read = 1;
      };
  
      data.push(message);
      updateDB(data);
    });

    
  };

  return userMessages;
};

exports.getUnreadMessages = (username) => {

  let messages = [];
  if (fs.existsSync('./src/db.json')) {
    messages = require('../db.json');
  } else {
    console.log('The database file does not exist');
  };

  let userMessages = [];
  let data = [];

  if (messages != []) {
    messages.forEach((message) => {
    
      if (message.receiver == username && message.read == 0) {
  
        userMessages.push(message);
        message.read = 1;
      };
  
      data.push(message);
      updateDB(data);
    });
  
    return userMessages;
  };

};

var data = [];

//Add a new message to the (temporary) database
exports.postMessage = (message) => {

  let data = [];
  if (fs.existsSync('./src/db.json')) {
    data = require('../db.json');
  }

  message["id"] = Date.now();
  message["read"] = 0;
  data.push(message);
  updateDB(data);
};

exports.deleteMessage = (message) => {
  if (fs.existsSync('./src/db.json')) {
    data = require('../db.json');
  } else {
    console.log('The database file does not exist');
  };

  let messageFound = false;
  for (let [i, item] of data.entries()) {
    if (message.id == item.id && message.sender == item.sender
      && message.receiver == item.receiver && message.content == item.content) {
        data.splice(i, 1);
        messageFound = true;
      }
  };

  if (!messageFound) {
    console.log('The data you entered does not fit any message')
  } else {
    updateDB(data);
  };
}

function updateDB (data) {
  fs.writeFile('./src/db.json', JSON.stringify(data, null, 2), (err) => {
    if (err) {
      console.log(err);
    };
    console.log('File updated');
  });
};

