messagesController = require('../controllers/messagesController');

//Function will be called from bootstrap
exports.appRoute = router => {

    router.get('/messages', messagesController.getMessagesController);

    router.get('/userMessages', messagesController.getUserController);

    router.get('/unreadMessages', messagesController.getUnreadController);
    
    router.post('/newMessage', messagesController.postMessageController);

    router.delete('/deleteMessage', messagesController.deleteMessageController);
};